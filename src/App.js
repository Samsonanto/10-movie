import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Body from './Body/Body.js';
import Navbar from './Navbar';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: "",
      searchList: [],
      myList: []
    }
  }
  searchHandler = (e) => {
    this.setState({
      search: e.target.value
    });
  }
  apiHandle = (e) => {
    e.preventDefault();
    let search = this.state.search;
    axios.get("http://www.omdbapi.com/?apikey=3811e0b6&s=" + search).then((response) => {
      this.setState({
        searchList: response.data.Search
      });
    })
      .catch((error) => {
        // handle error
        console.log(error);
      })
      .finally(() => {
        // always executed
        this.setState({
          search: ""
        });

      });
  }

  addMovie = (val) => {

    let list;
    list = this.state.myList.filter((x) => !(x === val));
    this.setState({ myList: [...list, val] });

    console.log(list);
  }

  renderMyMovie = (e) => {

    e.preventDefault();

    let list = [];
    this.state.myList.forEach((x) => {

      axios.get("http://www.omdbapi.com/?apikey=3811e0b6&i=" + x).then((res) => {

        let Title = res.data.Title;
        let Year = res.data.Year;
        let imdbID = res.data.imdbID;
        let Poster = res.data.Poster;
        let Type = res.data.Type;
        let Fav = true;
        let tmp = {
          Title: Title,
          Year: Year,
          imdbID: imdbID,
          Poster: Poster,
          Type: Type,
          Fav: Fav
        };
        list.push(tmp);
        this.setState({ searchList: list });
        console.log(list)
      }).catch((error) => {
        // handle error
        console.log(error);
      });
    });

  }

  removeMovie = (val)=>{

    let list = [];
    list = this.state.myList.filter( x => !(x===val) );
    this.setState({ myList : list } );

    list = this.state.searchList.filter( x => !(x.imdbID === val) );
    this.setState({ searchList : list});

  }
  render() {
    return (
      <div className="App" >

        <Navbar searchVal={this.state.search} handler={this.apiHandle} handleChange={this.searchHandler} myMovies={this.renderMyMovie} />

        <div className="container">
          <div className="row" >
            <Body value={this.state.searchList} addMovie={this.addMovie} removeMovie={this.removeMovie} />
          </div>

        </div>
      </div>
    );
  }
}

export default App;
